import { UserSegmentLookup, UserSegmentReference } from "common";

export class GASegmentPushTemplate implements CampaignTemplateComponent {

    @title('Facebook Pixel ID')
    fbPixelId:String

    @header('Additional Events to Track')
    submitApplication:Boolean = false;
    viewContent:Boolean = false;
    contact:Boolean = false;
    completeRegistration:Boolean = false;

    run(context: CampaignComponentContext) {
        return {
        };
    }
}