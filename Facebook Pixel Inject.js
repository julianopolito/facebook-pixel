(function () {
    function buildBindId(context) {
        return `${context.campaign}:${context.experience}`;
    }
    function apply(context, template) {
        Evergage.sendStat({
            campaignStats: [
                {
                    control: false,
                    experienceId: context.experience,
                    stat: "Impression"
                }
            ]
        });
        console.log('trying fb pixel injected jp')

        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded('body')
            .then((element) => {
                var ell = Evergage.cashDom('#fbpixelis');
                console.log('ell', ell);
                if(ell.length == 0){
                    !function(f,b,e,v,n,t,s)
                    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window,document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                    fbq('init', context.fbPixelId); 
                    fbq('track', 'PageView');
                    Evergage.cashDom('body').append('<span id="fbpixelis"></span>')
                    if(context.submitApplication)fbq('track', 'SubmitApplication');
                    if(context.viewContent)fbq('track', 'ViewContent');
                    if(context.contact)fbq('track', 'Contact');
                    if(context.completeRegistration)fbq('track', 'CompleteRegistration');

                    console.log('fb pixel injected jp');
                }

            });
    }

    function reset(context, template) {

    }

    function control() {
        Evergage.sendStat({
            campaignStats: [
                {
                    control: false,
                    experienceId: context.experience,
                    stat: "Impression"
                }
            ]
        });
        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded('body')
            .then((element) => {
                const html = template(context);
                Evergage.cashDom('body').append(html);
                console.log('fb tpl', tpl);
            });
    }

    registerTemplate({
        apply: apply,
        reset: reset,
        control: control
    });

})();
